# Respond-Reto-DevOps

Corresponde a la respuesta a la Prueba Técnica de la Empresa CLM Consultores.
Fuente: https://gitlab.com/sacaci.cl/reto-devops

# Supuestos

1er: Se asume que Docker y Kubernetes (minikube) instalados correctamente en la máquina a descargar esta respuesta.
Caso contrario, realizar la instalación mencionada en mi github (weblog): 
`<link>` https://github.com/mariofribla/Dev_K8S-Ubuntu18

2do: Se asume, que el repositorio entregado es estádico y se debe proceder aplicar lo solicitado sobre este código.

3er: Se asume, que estamos en un ambiente ideal para ejecución de este reto.

# Pre-Requisitos

Se deberá clonar este repositorio público a una maquina que tenga el suésto 1 instalado o ejecutado.

```sh
$ git clone https://gitlab.com/sacaci.cl/respond-reto-devops.git 

```

# Respuesta RETO 1: Dockerize la aplicación

Para la solución de este reto, asumi que el respositorio entregado debia ser dockerizado con una imagen ubuntu para generar la base para los siguientes retos.

Mencionado lo anterior, se describe los archivos del directorio `reto1_node/`

**Nota:** Para la ejecución de este reto, se menciona que la ejecución de la imagen Docker definida tendrá su tiempo de ejecución aproximadamente 10 minutos o menos si tienes una buena banda ancha, dado por el proceso de update del dockerización.

+ **Contenido**
En este directorio encontraremos solo el archivo `Dockerfile` que procederá a generar la imagen base pra los siguientes retos.

+ **Dockerfile**

```
FROM ubuntu:20.04                      # Define Imagen Docker base.
ENV DEBIAN_FRONTEND noninteractive     # Anula la interacción de apt-get.
MAINTAINER DEV mario.fribla@gmail.com  # Metadata.

RUN apt-get update                     # Realiza un Update sobre la imagen Docker Ubuntu.
RUN apt-get -y install npm             # Realiza la instalación del Gestor de Paquetes Javascripts.
RUN apt-get -y install wget            # Realiza la instalación del paquete Web Get o WWW Get.
RUN apt-get -y install unzip           # Realiza la instalación del paquete Descompresor Zip.

RUN wget https://gitlab.com/sacaci.cl/reto-devops/-/archive/master/reto-devops-master.zip     # Realiza la descarga del Fork realizado desde CLM Consultores.
RUN unzip reto-devops-master.zip       # Descomprime el proyecto Reto DevOps.

WORKDIR ./reto-devops-master           # Define el directorio de trabajo para este Dockerfile.

RUN npm install                        # Procede a la instalación de los paquete asociados al proyecto ret DevOps.

RUN npm run test                       # Ejecuta el Test del Proyecto Dev Ops.

EXPOSE 3000                            # Expone el puerto 3000

CMD node index.js                      # Define comando para la ejecución de servidor web Node.
```
+ **Creando la Imagen Docker de Reto 1:**
Ejecutar los siguientes comandos para generar la imagen Docker.

```sh
$ cd reto1_node/
$ docker build -t reto1:1  .
# Este proceso podria demorar varios minutos.
# Una ves finalizado validar su creación.
 $ docker images
 # En el despliegue deberá estar la imagen reto1:1
REPOSITORY    TAG   IMAGE ID   CREATED     SIZE
reto1         1     00000000   1 min ago   676MB
```
+ **Ejecución del Dockerización**
Para probar la dockerización, debemos ejecutar los siguinetes comandos:

```sh
$ docker run -d -p 3000:3000 --name webreto1 reto1:1
# Esta ejecución de entregará una <ID Container> que necesitaras posteriormente en este reto, por lo cual, copialo y mantenlo a la vista.
```
+ **Test**

```sh
$ curl -s localhost:3000/ 
{  "msg": "ApiRest prueba"}

$ curl -s localhost:3000/public
{  "public_token": "12837asd98a7sasd97a9sd7"}

$ curl -s localhost:3000/private
{  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}
```
+ **Deshabilitar Reto 1**
Necesitamos deshabilitar el docker para continuar con el desafio Reto 2, dado que estamos ocupando el puerto 3000, debemos ejecutar los siguinetes comandos:

```sh
$ docker stop <ID Container>    # Id que copiaste en la ejecución del docker.
$
$ docker rm <ID Container>      # Id que copiaste en la ejecución del docker.
$
```
+ **Conclusión**
Ya estamos listo para continuar con el **Reto 2**.
Espere que se hayan realizados todo slo pasos de esta etapa para el exito de los siguientes retos.

# Respuesta RETO 2: Docker Compose
Para la solución de este reto, tiene depemdencia con el **Reto 1** que debe estar exitosamente ejecutado.

Dentro de este directorio encontraremos la siguiente estructura:
+ **/**
En la raiz de este directorio, encontraemos el archivo `docker-compose.yml` que detallo a continuación:

```
version: "3.9"        # Versión del YAML para docker-compose.
services:             # Tag
  r2clmweb:           # Definción del servicio y nombre del  servicio.
    build:            # Configuración para la creación de un contenedor. Define el Dockerfile.
      context: .      # Ruta del directorio que contiene el Dockerfile.
      dockerfile: web/Dockerfile  # Nombre del archivo Dockerfile.
    container_name: ctn_clmweb    # Define el nombre del contenedor.
    tty: true
    ports:            # Expone la definición de los puertos (port’s) entre el contenedor y el host.
      - "3000:3000"   # Se definen que el puerto 3000 del contenedor se expone como 3000 en el host.

  r2lmnginx:                 # Definción del servicio y nombre del  servicio.
    container_name: ctn_clmnginx   # Define el nombre del contenedor.
    image: nginx:latest      # Define la imagen que se considera para la creación del contenedor.
    ports:                   # Expone la definición de los puertos (port’s) entre el contenedor y el host.
      - "80:80"              # Se definen que el puerto 80 del contenedor se expone como 80 en el host.
      - "443:443"            # Se definen que el puerto 443 del contenedor se expone como 443 en el host.
	                     # Se definen estos dos puertos (80/443) para poder aplicar seguridad y redireccionamiento.
    volumes:                 # Define la posibilidad de montar rutas del host o volúmenes hacia el contenedor.
      - "./nginx:/etc/nginx/conf.d" # Define la ruta local seae qiuvalente a la ruta del contendor.
    links:                   # Permite exponer servicios/alias a los contenedores, ya sean externos o internos.
      - "r2clmweb:clmserver" # Define que el servicio o contenedor sea reconocido como clmserver dentro del contenedor.

networks:                    # Define el tipo de red que tendrá el contenedor en creación.
  default:                   # Tag Network por default.
    external:                # Deifne una Netowrk Externa.
      name: net_clm          # Define que los contenedores estaran asociado network definida.
```


+ PreRequisitos de este Reto
Para continuar con este reto, debemos crear una Docker Network con la siguiente instrucción:

```sh
$ docker network create net_clm
# Con esta instrucción ha creado una red Brigde para este reto 
```
+ **Contenido**
Para comenzar debemos accder al directorio `reto2_dckcompose/`.
Dentro de este directorio encontraremos la siguiente estructura:

 + **nginx**
 Directorio que contiene todo lo relacionado con Web Server NGINX.
   + cert.crt y cert-key, archivos de corresponden al certificado necesario para el protocolo HTTPS requerido para este reto.
   + default.key, archivo con la configuración necesaria para el redireccionamiento del puerto 80 al puerto 443 para acceder al aplicativo.

 + **web**
 Directorio que contiene todo lo relacionado con Web Server NODE.
   + index.js, archivo con las instrucciones necesarias para cumplir con el requerimiento solicitado para este reto: `Asegurar el endpoint /private con auth_basic`
   + Dockerfile, archivo para la creación de la imagen y contenedor Docker para este reto.

```
FROM reto1:1                          # Define Imagen Docker base creado emn el Reto 1.
ENV DEBIAN_FRONTEND noninteractive    # Anula la interacción de apt-get.
MAINTAINER DEV mario.fribla@gmail.com # Metadata

WORKDIR /reto-devops-master           # Define el directorio de trabajo para este Dockerfile.

RUN npm install basic-auth            # Instala paquete para la autentificación.
RUN npm install express               # Es necessario reinstala
RUN npm install                       # Procede a la instalación de los paquete asociados al proyecto ret DevOps.

COPY web/index.js /reto-devops-master # Procede a la copia del archivo index.js a la imagen para habilitar la autenficiación para /private.

RUN npm run test                      # Ejecuta el Test del Proyecto Dev Ops.

EXPOSE 3000                           # Expone el puerto 3000
CMD node index.js                     # Define comando para la ejecución de servidor web Node.
```

+ **Ejecución del Dockerización**
Para probar la dockerización, debemos ejecutar los siguinetes comandos:

Debes estar ubicado en el directorio `reto2_dckcompose`
```sh
$ cd reto2_dckcompose
```
```sh
# Validar si existe la imagen Docker reto1:1.
$ docker images
# De existir procedemos con las siguiente instrucción:
$ docker-compose up -d
# Una ves finalizado la creación de lso contenedores, revisemos.
$ docker ps
# Deberan salir los contenedores  ctn_clmweb y  ctn_clmnginx .
CONTAINER ID   IMAGE                       COMMAND                  CREATED         STATUS         PORTS                                      NAMES
m07072021f     nginx:latest                "/docker-entrypoint.…"   5 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   ctn_clmnginx
f07072021m     reto2_dckcompose_r2clmweb   "/bin/sh -c 'node in…"   6 seconds ago   Up 4 seconds   0.0.0.0:3000->3000/tcp
```
+ **Test**
Para hacer el test deberas acceder desde un explorador a la ip o hostname donde esta desplegado este proyecto.

Por ejemplo:
```sh
$ firefox http://localhost
# Si no has accedido con anterioridad, Confirma continuar con Riesgo, deberá entregar:
{  "msg": "ApiRest prueba"}

$ firefox http://localhost/public
# Si no has accedido con anterioridad, Confirma continuar con Riesgo, deberá entregar:
{  "public_token": "12837asd98a7sasd97a9sd7"}

$ firefox http://localhost/private
# Si no has accedido con anterioridad, Confirma continuar con Riesgo, deberá entregar:
# Te pedirá acceso  Usuario: mario Clave: fribla
{  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}
```
+ **Conclusión**
Ya estamos listo para continuar con el **Reto 3**.
Espere que se hayan realizados todo slo pasos de esta etapa para el exito de los siguientes retos.

#Respuesta RETO 3:  Probar la aplicación en cualquier sistema CI/CD
Para realizar este reto, utilizaremos el directorio `respond-reto-devops/`, donde configuraremos el gitlab.

+ **PreRequisitos**
Para desarrollar este reto, deberas tener lo siguiente:
  + Tener instalado Git y ambeinte Docker.
  + Haber descargado este proyecto en un directorio.
  + Haber ejecutado el Reto 1 y Reto 2.
  + Poseer los contenedores en ejecución.

Cumpliendo estos prerequisitos, procedemos responde el Reto 3.

+ **Configurando GitLab**
Para dar solución a este Reto 3, crearemos en el directorio raiz (`respond-reto-devops/`) el archivo de de configuración de Gitlab-CI. Para hacer esto, realizaremos la siguiente configuración:

```
$ vi .gitlab-ci.yml
stages:
  - init
  - deploy
  - running

valited:
  stage: init
  script:
    - docker images | grep "reto1" && echo "Imagen Reto1 Existe..."
    - docker images | grep "reto2_dckcompose_r2clmweb" && echo "Imagen reto2_dckcompose_r2clmweb Existe..."
    - docker ps | grep "nginx:latest"  && echo "Contenedor nginx:latest en Ejecucion..."
    - docker ps | grep "reto2_dckcompose_r2clmweb" && echo "Contenedor nginx:latest en Ejecucion..."
git:
  stage: deploy
  script:
    - pwd | grep "respond-reto-devops" || echo "PATH Incorrecto.."
    - git pull https://gitlab.com/sacaci.cl/respond-reto-devops.git

runner:
  stage: running
  script:
    - pwd | grep "respond-reto-devops" || echo "PATH Incorrecto.."
    - cd reto2_dckcompose && echo "Cambio Direcorio para Actualizar Contenedor"
    - docker-compose down
    - docker-compose up -d
```
+ Configurando el Runner
Para configurar esta acción del Runner de Gitlab-CI, debes tener instalado el gitalab runners con el siguiente comando:

```sh
$ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
$ sudo chmod 777 /usr/local/bin/gitlab-runner
```
Una vez instalado, asociamos el gitlabRunner a nuestro Gitlab:
**Nota:** Estos datos pueden varias para el momento de probar.

```
$ gitlab-runner register
# URL: https://gitlab.com/
# Token: bETFBXAd9esd9bg94oEn
# Description: Respond Reto CLM
# TAG: <Enter>
# Executer: shell
$
$ nohup gitlab-runner run &
```

+ **Test**
Para realizar una prueba de la Integración Continua, se debe modificar y/o agregar algun archivo en el directorio de trabajo `respond-reto-devops/`. Po ejemplo:

  + Podemos modificar el index.js del directorio `reto2_dckcompose/web`, que en ves que nos entregue `Unauthorized` no s de cvomo respuesta `Usuario No Autorizado`. Para esto hacemos:

```sh
$ cd reto2_dckcompose/web
$ vi +31 index.js
# Reemplazamos 
    res.end('Unauthorized');
# por 
    res.end('Usuario No Autorizad');
#:wq
$ cd ../..
$ git add .
$ git commit -m "Cambio de index.js linea 31."
$ git push
```
Hasta este momento hemos activado el GitLab-CI asociado al directorio lo cual debemos ver el cambio reflejado en alguno s minutos en nuestro contenedor.

**Obs**: Lo ideal es tener dos ambientes para la replicación de este Reto.

# Respuesta RETO 4: Deploy en kubernetes
Lamentablemente por tiempo no alcance a documentar lo que habia avanzado y terminar este reto.

Saludos  y gracias por la oportunidad como el Reto Devops.

Atte
Mario Fribla G.

